#!/bin/bash
echo "Running edit mode tests"
"$UNITY_PATH/$UNITY_VERSION/Editor/Unity.exe" -projectPath "./" -batchmode -testPlatform editmode -runTests -debugCodeOptimization -logFile - -testResults "./testResults/editMode-results.xml" -enableCodeCoverage -coverageResultsPath "./testResults/coverage/" -coverageOptions enableCyclomaticComplexity -logFile - 
echo "Running play mode tests"
"$UNITY_PATH/$UNITY_VERSION/Editor/Unity.exe" -projectPath "./" -batchmode -testPlatform playmode -runTests -debugCodeOptimization -logFile - -testResults "./testResults/playMode-results.xml" -enableCodeCoverage -coverageResultsPath "./testResults/coverage/" -coverageOptions enableCyclomaticComplexity -logFile - 