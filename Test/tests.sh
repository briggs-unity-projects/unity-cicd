#!/bin/bash
echo "Test Platform: $TEST_PLATFORM"
"$UNITY_PATH/$UNITY_VERSION/Editor/Unity.exe" -projectPath "./" -runTests -testPlatform $TEST_PLATFORM -testResults "./$TEST_PLATFORM-results.xml" -logFile - -batchmode
UNITY_EXIT_CODE=$?
if [ $UNITY_EXIT_CODE -eq 0 ] then
  echo "Run succeeded, no failures occurred";
elif [ $UNITY_EXIT_CODE -eq 2 ] then
  echo "Run succeeded, some tests failed";
elif [ $UNITY_EXIT_CODE -eq 3 ] then
  echo "Run failure (other failure)";
else
  echo "Unexpected exit code $UNITY_EXIT_CODE";
fi
exit $UNITY_EXIT_CODE
