@echo off

SET TEST_PLATFORM=%1

echo "Formatting unit test report"
xml tr unity-cicd/Test/format.xslt %TEST_PLATFORM%-results.xml > nunit-%TEST_PLATFORM%-results.xml