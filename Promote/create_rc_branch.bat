@echo off

echo "# # # # # PROMOTING TO RELEASE CANDIDATE # # # # #"

SET VERSION=%1
SET GIT_URL=%2
SET PROJECT_PATH=%3
SET CI_PROJECT_ID=%4
SET API_TOKEN=%5
SET CI_ENV=%6
SET CI_BUILD_REF_NAME=%7
SET GITLAB_USER_NAME=%8

:: git checkout master
git checkout master
git fetch origin
git pull origin master

echo "MAJOR_VERSION: %VERSION%"

for /f %%i in ('git tag -l ""v%VERSION%.[0-9]*"" v:refname ^| tail -1') do set CURRENT_TAG_VERSION=%%i
:: SET CURRENT_TAG_VERSION = 'git tag -l "v%VERSION%.[0-9]*" v:refname | tail -1'
echo "CURRENT_TAG_VERSION: %CURRENT_TAG_VERSION%"

SET NEW_PATCH_VERSION=0
if "%CURRENT_TAG_VERSION%"=="" GOTO SKIP_GET_PATCH    
for /f %%i in ('echo %CURRENT_TAG_VERSION% ^| sed -e "s/v[0-9]\+\.[0-9]\+\.[0-9]\+\.//g" ') do set CURRENT_PATCH_VERSION=%%i
echo "CURRENT_PATCH_VERSION=%CURRENT_PATCH_VERSION%"
SET /A NEW_PATCH_VERSION = %CURRENT_PATCH_VERSION%+1

:SKIP_GET_PATCH

echo "NEW_PATCH_VERSION=%NEW_PATCH_VERSION%"
SET CI_NEW_VERSION=%VERSION%.%NEW_PATCH_VERSION%
echo "CI_NEW_VERSION=%CI_NEW_VERSION%"
SET CI_NEW_BRANCH=rc-%CI_NEW_VERSION%
echo "CI_NEW_BRANCH=%CI_NEW_BRANCH%"


:: TAG MASTER WITH VERSION NUMBER
git tag v%CI_NEW_VERSION%
echo "Username: %GITLAB_USER_NAME%"
git push http://JABriggs:%API_TOKEN%@gitlab.com%PROJECT_PATH% v%CI_NEW_VERSION%

:: CREATE RC BRANCH AND PUSH
echo "Checking out new branch - %CI_NEW_BRANCH%"
git checkout -b %CI_NEW_BRANCH%
:: echo "Setting new version"
echo %CI_NEW_VERSION% >> version.txt
:: Change version in project
echo "Commit branch"
git commit -am "Release branch %CI_NEW_BRANCH%"
echo "Pushing changes"
git push -q --set-upstream http://JABriggs:%API_TOKEN%@gitlab.com%PROJECT_PATH% %CI_NEW_BRANCH%

:: git checkout master
git checkout master