@echo off

echo "# # # # # CREATE MERGE REQUEST # # # # #"

SET GIT_URL=%1
SET CI_PROJECT_ID=%2
SET API_TOKEN=%3
SET CI_ENV=%4
SET CI_BUILD_REF_NAME=%5

GOTO MAIN

:: Create merge request
:merge_request
echo Branch Title: %mr_title%
echo "{""target_branch"":""%target_branch%"",""source_branch"":""%source_branch%"",""project_id"":%CI_PROJECT_ID%,""title"":""%mr_title%"",""state"":""opened""}"
set mr_json="{""target_branch"":""%target_branch%"",""source_branch"":""%source_branch%"",""project_id"":%CI_PROJECT_ID%,""title"":""%mr_title%"",""state"":""opened""}"
echo Data: %mr_json%

for /f %%i in ('curl -v -X POST --header "content-type:application/json" --header "PRIVATE-TOKEN:%API_TOKEN%" "%GIT_URL%/projects/%CI_PROJECT_ID%/merge_requests" --data-raw %mr_json% ') do set response=%%i
echo Response: %response%

echo %target_branch%

if "%target_branch%"=="master" GOTO END_MERGE_REQUEST_MASTER
if "%target_branch%"=="STAGE" GOTO END_MERGE_REQUEST_STAGE
if "%target_branch%"=="PRODUCTION" GOTO END_MERGE_REQUEST_PRODUCTION




:: Update merge request
:update_merge_request
echo Branch Title: %mr_title%
set mr_json="{""target_branch"":""%target_branch%"",""source_branch"":""%source_branch%"",""project_id"":%CI_PROJECT_ID%,""title"":""%mr_title%"",""state"":""opened""}"
echo Data: %mr_json%

for /f %%i in ('curl -v -X POST --header "content-type:application/json" --header "PRIVATE-TOKEN:%API_TOKEN%" "%GIT_URL%/projects/%CI_PROJECT_ID%/merge_requests" --data-raw %mr_json% ') do set response=%%i
echo Response: %response%

if "%target_branch%"=="master" GOTO END_UPDATE_MERGE_REQUEST_MASTER
if "%target_branch%"=="STAGE" GOTO END_UPDATE_MERGE_REQUEST_STAGE
if "%target_branch%"=="PRODUCTION" GOTO END_UPDATE_MERGE_REQUEST_PRODUCTION





:: Get open merge requests
:get_open_merge_requests 
echo Get a list of open merge requests for the project

for /f %%i in ('curl -v -X GET --header "content-type:application/json" --header "PRIVATE-TOKEN:%API_TOKEN%" "%GIT_URL%/projects/%CI_PROJECT_ID%/merge_requests?state=opened" ^| grep -Po "\"iid\":([0-9]*)" ^| sed -e "s/""iid""://" ') do set openMergeRequests=%%i


if "%target_branch%"=="master" GOTO END_GET_OPEN_MERGE_REQUESTS_MASTER
if "%target_branch%"=="STAGE" GOTO END_GET_OPEN_MERGE_REQUESTS_STAGE
if "%target_branch%"=="PRODUCTION" GOTO END_GET_OPEN_MERGE_REQUESTS_PRODUCTION









:MAIN

echo PROMOTE_STAGE: %CI_ENV%

REM Master Stuff
if NOT %CI_ENV%==master GOTO NOT_MASTER
echo Creating merge request for master
set mr_title=WIP: %CI_BUILD_REF_NAME%
set target_branch=%CI_ENV%
set source_branch=%CI_BUILD_REF_NAME%
call :merge_request
:END_MERGE_REQUEST_MASTER

EXIT /B 0




:NOT_MASTER
if NOT %CI_ENV%==RELEASE GOTO NO_REQUEST
:: Release Stuff

:: Stage merge request
set mr_title=STAGE Deploy: %CI_BUILD_REF_NAME%
set target_branch=STAGE
set source_branch=RELEASE

call :get_open_merge_requests
:END_GET_OPEN_MERGE_REQUESTS_STAGE

if "%openMergeRequests%"=="0" GOTO NEW_MERGE_REQUEST_STAGE
if NOT "%openMergeRequests%"=="0" GOTO EXISTING_MERGE_REQUEST_STAGE

:NEW_MERGE_REQUEST_STAGE
ECHO "Creating merge request for STAGE"
call :merge_request
:END_MERGE_REQUEST_STAGE
GOTO END_MERGE_REQUEST_IF_STAGE

:EXISTING_MERGE_REQUEST_STAGE
ECHO "Updating merge request for STAGE"
call :update_merge_request
:END_UPDATE_MERGE_REQUEST_STAGE
GOTO END_MERGE_REQUEST_IF_STAGE

:END_MERGE_REQUEST_IF_STAGE

:: Production merge request
set mr_title=PRODUCTION Deploy: %CI_BUILD_REF_NAME%
set target_branch=PRODUCTION
set source_branch=RELEASE

call :get_open_merge_requests
:END_GET_OPEN_MERGE_REQUESTS_PRODUCTION

if "%openMergeRequests%"=="0" GOTO NEW_MERGE_REQUEST_PRODUCTION
if NOT "%openMergeRequests%"=="0" GOTO EXISTING_MERGE_REQUEST_PRODUCTION

:NEW_MERGE_REQUEST_PRODUCTION
ECHO "Creating merge request for PRODUCTION"
call :merge_request
:END_MERGE_REQUEST_PRODUCTION
GOTO END_MERGE_REQUEST_IF_PRODUCTION

:EXISTING_MERGE_REQUEST_PRODUCTION
ECHO "Updating merge request for PRODUCTION"
call :update_merge_request
:END_UPDATE_MERGE_REQUEST_PRODUCTION

GOTO END_MERGE_REQUEST_IF_PRODUCTION


:END_MERGE_REQUEST_IF_PRODUCTION
EXIT /B 0

:NO_REQUEST
echo Merge Request Failed.
EXIT /B -1


rem # # # # # USEFUL STUFF FOR LATER
rem call :get_open_merge_requests
rem :END_GET_OPEN_MERGE_REQUESTS
rem echo Open merge request: %openMergeRequests%
