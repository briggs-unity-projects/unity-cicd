# Unity CI/CD

The purpose of this Unity CI repository is to provide CI/CD scripts to a Unity project. When using the .gitlab-ci.yml file in your unity project, your project should be able to build and deploy to WebGL, Win64, Linux and OSX.

The example .gitlab-ci.yml file in this repository is just a demonstration on how the scripts are to be called. If you feel, you can just use your own scripts.

Within this repository is an Editor directory, which needs to be dragged into your unity project, under the scripts folder.

For the deployment to Itch.io to work within GitLab, an environmental variable including your itch.io key is needed. This is done by going to your project in GitLab, and then going Settings>Variables>Add Variable and then adding a variable with the name BUTLER_API_KEY.

## Future Updates
Unit Testing