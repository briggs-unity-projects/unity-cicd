@echo off

REM Constants
SET UNITY_PATH=%1
SET UNITY_VERSION=%2
echo Unity Path: %UNITY_PATH%
echo Unity Version: %UNITY_VERSION%
echo Starting Build Process
"%UNITY_PATH%/%UNITY_VERSION%/Editor/Unity.exe" -quit -batchmode -projectPath "./" -executeMethod BuildScript.PerformBuildWebGL -logFile -