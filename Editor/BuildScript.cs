﻿using UnityEditor;
using UnityEngine;
using UnityEditor.Build.Reporting;

public class BuildScript 
{
    private static string[] scenes = new string[3] {
        "Assets/Scenes/Menus/MainMenu.unity",
        "Assets/Scenes/Menus/PartyCreation.unity",
        "Assets/Scenes/GameWorld/MainWorld.unity"
    };

    static void BuildReport(BuildPlayerOptions buildPlayerOptions)
    {
        BuildReport report = BuildPipeline.BuildPlayer(buildPlayerOptions);
        BuildSummary summary = report.summary;

        if (summary.result == BuildResult.Succeeded)
        {
            Debug.Log("Build succeeded: " + summary.totalSize + " bytes");
            EditorApplication.Exit( 0 );
        }

        if (summary.result == BuildResult.Failed)
        {
            Debug.Log("Build failed");
            EditorApplication.Exit( 1 );
        }

    }

    [MenuItem("Custom Utilities/Build WebGL")]
    static void PerformBuildWebGL()
    {
        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
        buildPlayerOptions.scenes = scenes;
        buildPlayerOptions.locationPathName = "./Builds/WebGL/";
        buildPlayerOptions.target = BuildTarget.WebGL;
        buildPlayerOptions.options = BuildOptions.None;
        
        BuildReport(buildPlayerOptions);
    }

    [MenuItem("Custom Utilities/Build Win64")]
    static void PerformBuildWin64()
    {
        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
        buildPlayerOptions.scenes = scenes;
        buildPlayerOptions.locationPathName = "./Builds/Win64/Application.exe";
        buildPlayerOptions.target = BuildTarget.StandaloneWindows64;
        buildPlayerOptions.options = BuildOptions.None;
        
        BuildReport(buildPlayerOptions);
    }


    [MenuItem("Custom Utilities/Build Win32")]
    static void PerformBuildWin32()
    {
        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
        buildPlayerOptions.scenes = scenes;
        buildPlayerOptions.locationPathName = "./Builds/Win64/Application.exe";
        buildPlayerOptions.target = BuildTarget.StandaloneWindows;
        buildPlayerOptions.options = BuildOptions.None;
        
        BuildReport(buildPlayerOptions);
    }

    [MenuItem("Custom Utilities/Build Linux64")]
    static void PerformBuildLinux()
    {
        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
        buildPlayerOptions.scenes = scenes;
        buildPlayerOptions.locationPathName = "./Builds/Linux/Application.x86_64";
        buildPlayerOptions.target = BuildTarget.StandaloneLinux64;
        buildPlayerOptions.options = BuildOptions.None;
        
        BuildReport(buildPlayerOptions);
    }

    [MenuItem("Custom Utilities/Build MacOS")]
    static void PerformBuildOSX()
    {
        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
        buildPlayerOptions.scenes = scenes;
        buildPlayerOptions.locationPathName = "./Builds/OSX/Application.app";
        buildPlayerOptions.target = BuildTarget.StandaloneOSX;
        buildPlayerOptions.options = BuildOptions.None;
        
        BuildReport(buildPlayerOptions);
    }

}