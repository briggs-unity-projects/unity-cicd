@echo off

REM Constants
SET USERNAME=%1
SET PROJECT=%2
SET BETA=%3
FOR /F %%i IN (file.txt) DO set VERSION=%%i+BETA
echo Username: %USERNAME%
echo Project: %PROJECT%
echo Version: %VERSION%
butler push .\Builds\OSX\ %USERNAME%/%PROJECT%:OSX-%VERSION%
echo Finished uploading project!
butler status %USERNAME%/%PROJECT%:OSX-%VERSION%