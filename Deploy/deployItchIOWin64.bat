@echo off

REM Backup application folder
MOVE Builds\Win64\Application_BackUpThisFolder_ButDontShipItWithYourGame Win64BackUp

REM Constants
SET USERNAME=%1
SET PROJECT=%2
SET BETA=%3
FOR /F %%i IN (file.txt) DO set VERSION=%%i+BETA
echo Username: %USERNAME%
echo Project: %PROJECT%
echo Version: %VERSION%
butler push .\Builds\Win64\ %USERNAME%/%PROJECT%:Win64-%VERSION%
echo Finished uploading project!
butler status %USERNAME%/%PROJECT%:Win64-%VERSION%